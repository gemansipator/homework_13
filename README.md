# Getting Started with Create React App

Домашнее задание №13 (к уроку от 31.07.2023)
ТЕКСТ ЗАДАНИЯ
Расширить функционал приложения, созданного в ДЗ №11 (к уроку от 24.07.2023):

Добавить к карточкам товаров строку поиска по заголовку
Добавить к карточкам товаров фильтр по категории 
(с использованием тега select). Первым пунктом в селекте должен 
быть "Все товары", который выбран по умолчанию и при выборе его
отображаются все доступные категории.